# This is read me.

Get Started.

```bash
git config --global user.name "Jayden Aung"
git config --global user.email "jaydenaung@gmail.com"
```
---
## Create a new repository

Execute the following:

```bash
git clone https://gitlab.com/jaydenaung/deploy-gke-in-5.git
cd deploy-gke-in-5
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
---
## Push an existing folder

```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/jaydenaung/deploy-gke-in-5.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
---
## Push an existing Git repository

```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/jaydenaung/deploy-gke-in-5.git
git push -u origin --all
git push -u origin --tags
```

